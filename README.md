[![tickets](https://img.shields.io/badge/tickets-here-informational?style=flat-square&color=0062CC)](https://todo.sr.ht/~tmpod/black-lite)
[![mailing lists](https://img.shields.io/badge/mailing_lists-here-informational?style=flat-square&color=0062CC)](https://lists.sr.ht/~tmpod/black-lite)

# black-lite

A simple plugin for [lite](https://github.com/rxi/lite) that adds a command and keybind to run
[`black`](https://github.com/psf/black) on the current file.


## Installing

Navigate to your `data/plugins` directory and then use one of the following methods:

1. Just download the plugin file into your plugins folder.
    ```
    curl -O https://git.sr.ht/~tmpod/black-lite/blob/master/black.lua
    ```

2. Clone this repository into your plugins folder. This allows you to easily update the plugin by doing `git pull` but
it fetches unnecessary files, such as this README.
    ```
    git clone https://git.sr.ht/~tmpod/black-lite
    ```


## Usage

The plugin is very easy to use, you just have to either run the `Black: Format` command, or hit `Alt+L`.

A message in the status bar will appear letting you know if the `black` ran successfully or not.


## Configurations

This plugin has two configuration fields.
You can set these configurations by editing your `init.lua` file in the `data/user` lite directory and modifying the
`core.config` table.
Each field name should be prefixed with `black_`, for example, `black_args`.
Here's a description of each valid configuration field:

| Name | Type | Description |
| :--- | ---- | ----------- |
| `args` | `string[]` | A table containing extra arguments to pass to the `black` command. It must only contain strings which will be joined by spaces and inserted onto the command. |
| `keybind` | `string` | The key-combo to register for the `Black: Format` command shortcut. |

Example:
```lua
local config = require "core.config"

-- Lines can be 120 chars long
config.black_args = {"-l 120"}
```


## License

This plugin is licensed under MIT. The license file can be found [here](./LICENSE).

