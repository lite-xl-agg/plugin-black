-----------------------------------------------------------------------------------------------------
-- Python's formatter `black` integration for the `lite` text editor (https://github.com/rxi/lite) --
-- Made with <3 by Tmpod                                                                           --
-----------------------------------------------------------------------------------------------------

-- Imports --
local core = require "core"
local command = require "core.command"
local keymap = require "core.keymap"
local config = require "core.config"

-- TODO: Think about something to solve the issue of slow auto-reloading that doesn't involve copying the whole plugin here.

-- Configurations --
-- Defaults
-- Extra arguments to pass. The table must contain strings which will be "joined" with spaces.
config.black_args = {}
-- The keybind to register for the "black: format" command.
config.black_keybind = "alt+l"


-- Logic --
local function format()
  -- First we save the file if it is unsaved or dirty
  local doc = core.active_view.doc
  if doc:get_name() == "unsaved" or doc:is_dirty() then
    doc:save()
  end

  -- Then we run black
  -- Shadowing ok, but it's ok :P
  local ok, _, code = os.execute(
    string.format(
      "black %s %s",
      table.concat(config.black_args, " "),
      doc:get_name()
    )
  )
  if not ok then
    core.log("Failed to format file! Return code is " .. code)
    return
  end

  core.log("Reformatted file")
end


-- Register the command
command.add("core.docview", {["black:format"] = format})

-- And a shotcut
keymap.add {
    [config.black_keybind] = "black:format"
}

